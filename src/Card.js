import React from 'react';

class Card extends React.Component {
    
    handleButtonClick = (event) => {
        console.log("id clicked = " + this.props.gameID);
        this.props.onClick(this.props.gameID);
    }

    handleGameEdit = (event) => {
        this.props.editGame(this.props.gameID);
    }

    render () {
        return (
            <div className="card" style={{width: '18rem'}}>
                <img src={this.props.gameImage} className="card-img-top" alt="Oops"/>
                <div className="card-body">
                    <h5 className="card-title">{this.props.gameTitle}</h5>
                    <p className="card-text">{this.props.gameGenre}</p>
                    <button href="#" onClick={this.handleButtonClick} className="btn btn-primary">{this.props.buttonText}</button>
                    <button href="#" onClick={this.handleGameEdit} className="btn btn-primary">Edit</button>
                </div>
            </div>
        );
    }

}

export default Card;