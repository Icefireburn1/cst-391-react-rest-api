import React from 'react';
import axios from 'axios';

class OneGame extends React.Component {

    handleEditGame = (gameID) => {
        console.log("Edit this id = ",this.props.game.id);
        this.props.editGame(gameID);
    }

    handleDeleteGame = () => {
        console.log("Deleting game = ", this.props.game);
        axios.delete("http://localhost:8080/service/games/" + this.props.game.id, this.props.game.id)
        .then(result => {
            console.log(result);
            console.log(result.data);
            alert("Game deleted");
            this.navigateTo();
        })
        .catch ((error) => {
            console.log("Error deleting game: ", error);
            alert("Error: Game failed to delete");
        })
    }

    navigateTo() {
        window.location.replace("http://localhost:3000");
    }

    render() {
        console.log("Props for OneGame=",this.props);
        return(
            <div className="row">
                <h2>Game Details for {this.props.game.title}</h2>
                    <div className="row">
                        <div className="card">
                            <img src={this.props.game.image} className="card-img-top" alt={this.props.game.title}/>
                            <div className="card-body">
                                <h5 className="card-title">{this.props.game.title}</h5>
                                <p className="card-text">{this.props.game.genre}</p>
                                <p className="card-text">${this.props.game.cost}</p>
                                <button href="#" className="btn btn-primary" onClick={this.handleEditGame}>Edit</button>
                                <button href="#" className="btn btn-light" onClick={this.handleDeleteGame}>Delete</button>
                            </div>
                        </div>
                    </div>

            </div>
        )
    }
}

export default OneGame;