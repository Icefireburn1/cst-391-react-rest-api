import React from 'react';
import FormInput from './FormInput';
import axios from 'axios';

class NewGame extends React.Component {

    state = {
        title: "Title",
        genre: "Genre",
        cost: -1,
        image: "https://th.bing.com/th/id/R.1bbcd06ce1b0614526ba56defcd0eb73?rik=kyr7NIGVc6ZejQ&pid=ImgRaw&r=0",
    }

    updateTitle = (t) => {
        this.setState({title: t});
        console.log("State of form = ",this.state);
    }

    updateGenre = (t) => {
        this.setState({genre: t});
        console.log("State of form = ",this.state);
    }

    updateCost = (t) => {
        this.setState({cost: t});
        console.log("State of form = ",this.state);
    }

    updateImage = (t) => {
        this.setState({image: t});
        console.log("State of form = ",this.state);
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        console.log("Final submit = ",this.state);
        this.saveGame(this.state);
    }

    saveGame = async (game) => {
        axios.post('http://localhost:8080/service/games', game)
        .then(result => {
            console.log(result);
            console.log(result.data);
            alert("Game created");
            this.navigateTo();
        }).catch((error) => {
            console.log("Problem adding game: " + error);
            alert("Error: Could not make game");
        });
    }

    navigateTo() {
        window.location.replace("http://localhost:3000");
    }

    render() {
        return (
            <div className="container">
                <form onSubmit = {this.handleFormSubmit}>
                    <div className = "form-group">
                        <h1>Add a new game</h1>
                            <FormInput id ="gameTitle" title="Title" onChange={this.updateTitle}/>
                            <FormInput id ="gameGenre" title="Genre" onChange={this.updateGenre}/>
                            <FormInput id ="gameCost" title="Cost" onChange={this.updateCost}/>
                            <FormInput id ="gameImage" title="Image" onChange={this.updateImage}/>
                    </div>
                    <button type="button" onClick={this.navigateTo} className="btn btn-light">Cancel</button>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}

export default NewGame;