import React from 'react';

class FormInput extends React.Component {

    state = {inputData: ""};

    handleChangeData = (e) => {
        this.setState({inputData: e.target.value});
        console.log("contents of the input from FormInput = ", this.state.inputData);
        this.props.onChange(this.state.inputData);
    }

    render () {
        return (
        <div>
            <label htmlFor={this.props.id}>{this.props.title}</label>
            <input type="text" onBlur={this.handleChangeData} onChange={this.handleChangeData} className="form-control" id={this.props.id} placeholder={this.props.placeholder} />
        </div>)
    }
}

export default FormInput;