import React from 'react';
import "./App.css";
import dataSource from './dataSource';
import GameList from './GameList';
import Navbar from './Navbar'
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import OneGame from './OneGame';
import EditGame from './EditGame';
import NewGame from './NewGame';

const history = createBrowserHistory();

class App extends React.Component {

    state = {gamelist : [], searchphrase: "", currentlySelectedGameId: 1};

    updateSingleGame = (id) => {
        console.log("updateSingle =", id);
        var indexNumber = 0;
        for (var i = 0; i < this.state.gamelist.length; i++) {
            if (this.state.gamelist[i].id === id)
                indexNumber = i;
        }
        this.setState({currentlySelectedGameId: indexNumber});
        history.push('/games/' + id);
        console.log("Currently selected set to = ", this.state.currentlySelectedGameId);
    }

    componentDidMount() {
        this.loadGames();
    }

    loadGames = async () => {
        console.log("Checking...");
        const response = await dataSource.get('/games');
        console.log("Found: ", response);
        this.setState({gamelist: response.data});
    }

    editGame = (gameID) => {
        console.log("App. Edit currentlySelectedId = ", gameID);
        var indexNumber = 0;
        for (var i = 0; i < this.state.gamelist.length; i++) {
            if (this.state.gamelist[i].id === gameID)
                indexNumber = i;
        }
        this.setState({currentlySelectedGameId: indexNumber});
        history.push('/edit/' + gameID);
        console.log("state", this.state);
    }


    render() {
        return (
            <Router history={history}>
            <div>
                <Navbar />
                <Switch>
                <Route exact path="/" render = {() => {
                    return (
                        <div>
                            <GameList gamelist={this.state.gamelist} onClick={this.updateSingleGame} onEditGame={this.editGame}/>
                        </div>
                    )
                }}
                />
                <Route exact path="/new" component={NewGame}/>
                <Route exact path ="/games/:gameId" render={() => <OneGame game={this.state.gamelist[this.state.currentlySelectedGameId]} editGame={this.editGame}/>}/>
                <Route exact path ="/edit/:gameId" render={() => <EditGame game = {this.state.gamelist[this.state.currentlySelectedGameId]}/>}/>
                </Switch>
            </div>
            </Router>

        )
    }


}

export default App;