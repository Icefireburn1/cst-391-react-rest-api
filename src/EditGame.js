import React from 'react';
import FormInput from './FormInput';
import axios from 'axios';

class EditGame extends React.Component {

    state = {
        id: this.props.game.id,
        title: this.props.game.title,
        genre: this.props.game.genre,
        cost: this.props.game.cost,
        image: this.props.game.image,
    }

    updateTitle = (t) => {
        this.setState({title: t});
        console.log("State of form = ", this.state);
    }

    
    updateGenre = (t) => {
        this.setState({genre: t});
        console.log("State of form = ", this.state);
    }
 
    updateCost = (t) => {
        this.setState({cost: t});
        console.log("State of form = ", this.state);
    }

    
    updateImage = (t) => {
        this.setState({image: t});
        console.log("State of form = ", this.state);
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        console.log("Final submit: ", this.state);
        this.saveGame(this.state);
    }

    saveGame = async(game) => {
        console.log("new game data=", game);
        axios.put("http://localhost:8080/service/games/" + game.id, game)
        .then(result => {
            console.log(result);
            console.log(result.data);
            alert("Game updated");
            this.navigateTo();
        })
        .catch ((error) => {
            console.log("Error saving game: ", error);
            alert("Error: Game failed to update");
        })
    }

    navigateTo() {
        window.location.replace("http://localhost:3000");
    }

    render () {
        return (
            <div className = "container">
                <form onSubmit = {this.handleFormSubmit}>
                    <div className = "form-group">
                        <h1>Edit a game</h1>
                            <FormInput id ="gameTitle" title="Title" value={this.props.game.title} onChange={this.updateTitle}/>
                            <FormInput id ="gameGenre" title="Genre" value={this.props.game.genre} onChange={this.updateGenre}/>
                            <FormInput id ="gameCost" title="Cost" value={this.props.game.cost} onChange={this.updateCost}/>
                            <FormInput id ="gameImage" title="Image" value={this.props.game.image} onChange={this.updateImage}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                    <button type="button" onClick={this.navigateTo} className="btn btn-light">Cancel</button>
                </form>
            </div>
        )
    }

}

export default EditGame;