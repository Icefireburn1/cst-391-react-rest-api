import React from 'react';
import Card from './Card';

class GameList extends React.Component {
    
    handleSelectOne = (gameID) => {
        console.log("Selected id = ", gameID);
        this.props.onClick(gameID);
    }

    handleEditGame = (gameID) => {
        console.log("Edit this id = ",gameID);
        this.props.onEditGame(gameID);
    }

    render() {
        const games = this.props.gamelist.map(
            (game) => {
                return (<div key={game.id}><Card key={game.id} gameID={game.id} gameTitle={game.title} gameCost={game.cost} gameImage={game.image} buttonText="OK" gameGenre={game.genre} onClick={this.handleSelectOne} editGame={this.handleEditGame} /></div>);
            }
        );

        return (<div className="container">{games}</div>)
    }
}

export default GameList;